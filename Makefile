# Copyright (c) 2001 by Sun Microsystems, Inc.
# All rights reserved.

all: abicheck

abicheck: abicheck.pl
	cat abicheck.pl > abicheck
	chmod +x abicheck

test: abicheck FRC
	cd test; $(MAKE)

clean:
	rm -f abicheck

clobber: clean
	cd test; $(MAKE) clobber

DIR = abicheck-0.0

tarball: clobber
	rm -f $(DIR); ln -s . $(DIR)
	tar -cvf abicheck.tar `find $(DIR)/* -type f | egrep -v 'SCCS|CKPT|abicheck.tar'`
	rm -f $(DIR)

FRC:
	
