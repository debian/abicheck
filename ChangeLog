Version 1.1 RELEASE (6/18/2002)

- Added information from GLIBC sourcebase (2.3) wrt the new GLIBC_PRIVATE 
  version name.  This is a bootstrapping measure to push back this
  important info to the tool running on earlier GLIBC releases.

- Fixed a bug in parsing the LD_DEBUG output in newer GLIBC dynamic
  linkers:  binding file abc to xyz: normal symbol `foo' [GLIBC_2.0]
  earlier:  binding file abc to xyz: symbol `foo' [GLIBC_2.0]

- Fixed some of the tests on more recent GLIBC environments, it turns
  out that __open() is a public symbol, so not a good test for a
  private binding.

- Added an option -a to search through the binaries to be checked,
  collect all of the shared libraries, and then behave as though their
  basenames had been specified via the -l option.  This is a simple way
  to check all "internal" bindings rather than just the direct ones.
  Useful if the internal shared libraries do not have their
  dependencies recorded.

- Added an option -j <n> to run <n> jobs in parallel.  Useful speedup
  on SMP machines.

Version 1.0 RELEASE (2/7/2002)

- Initial release.
